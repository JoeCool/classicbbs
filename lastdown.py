#!/usr/bin/python
# Last Call BBS - Classic BBS Downloader
#
# Downloads and merges a variety of door games for Last Call BBS into a single js file.
# The resulting js file is an accessible 'classic' bbs that can be visited to play
# all the games that the tool manages.
#
# This code and the boilerplate is licensed under MIT/X
#
# Copyright (c) 2022 almostsweet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Changelog:
# v0.10
#  - First implementation.
#  - Initial release on reddit.
# v0.20
#  - Remove use strict for doors that dont use it.
#  - URLs now point to the latest .js files instead of specific commits.
#  - Added Subleq
# v0.21
#  - Added Crossroads
#  - Fixed issue with defaults not being restored properly, required an api change
# v0.22
#  - Apparently only had to remove use strict from the top of the boilerplate
#  - Cursor position fixed on door game menu
#  - Added Trade Wars 2057
#  - Fixed odd number of doors causing the door menu not to draw the final line
# v0.23
#  - Fixed formatting issues on door menu
# v0.24
#  - boilerplate automatically populated w/ save supported + func call details
#  - Added Monstermaze
#  - Better formatting for the console output of the lastdown tool
# v0.25
#  - Added a progress indicator to the console output of the lastdown tool
#  - Added SokoCode
# v0.26
#  - Added Hoos Starzu
# v0.27
#  - Added Hanabi (fireworks)
# v0.28
#  - Added Flappy Birb
# v0.29
#  - Added exaDOOM
# v0.30
#  - Added Empty Saloon
# v0.31
#  - Added Snake
#  - Added netMAZE
# v0.32
#  - Fixed the BBS clock
#  - Added the Sysop Pager, menu option P
#  - Cleaned up the menu a bit
#  - Added overlays
#  - Added an empty graffitti wall, menu option W
#  - Added an empty news, menu option N
#  - Added an empty mail, menu option  M
#  - Added an empty users, menu option  U
#  - Added expert mode toggle (hides the main menu)
# v0.33
#  - Added ACiD Draw
#  - Fixed Hoos overriding the name
# v0.34
#  - Fixed an issue for users of OneDrive
# v0.35
#  - Added CR-S01's Dungeons And Designs
# v0.36
#  - ACiD Draw now supports the save api
#
import os, re, sys, hashlib, six, codecs
from six.moves import urllib
from six.moves.urllib import request
from six.moves.urllib.parse import urlparse

VERSION='0.36'

finalpath = "\\Documents\\My Games\\Last Call BBS\\"
dest = os.environ['USERPROFILE']+finalpath

door_games = [
'https://bitbucket.org/almostsweet/lord/raw/main/LORD.js',
'https://bitbucket.org/almostsweet/breakout/raw/main/breakout.js',
'https://raw.githubusercontent.com/galactical1100/Subleq/main/subleq.js',
'https://bitbucket.org/almostsweet/crossroads/raw/main/crossroads.js',
'https://bitbucket.org/almostsweet/tw2057/raw/main/tw.js',
'https://raw.githubusercontent.com/Zwergesel/last-call-bbs-monster-maze/main/monstermaze.js',
'https://raw.githubusercontent.com/Werxzy/SokoCode/master/SokoCode.js',
'https://raw.githubusercontent.com/stellartux/hoos-starzu/master/hoos-starzu.js',
'https://raw.githubusercontent.com/arun867/last-call-bbs-server-hanabi/main/hanabi.js',
'https://gist.githubusercontent.com/micolous/b822a40674e5527aba42f0612a013faf/raw/flappy.js',
'https://raw.githubusercontent.com/vrman123/LastCallBBS/main/exaDOOM.js',
'https://raw.githubusercontent.com/icegoat9/lastcallbbs/main/emptysaloon.js',
'https://raw.githubusercontent.com/dabjulmaros/last-call-bbs-server-snake/master/dabjulmaros_snake.js',
'https://raw.githubusercontent.com/JavadocMD/last-call-bbs/main/netmaze.js',
'https://bitbucket.org/almostsweet/aciddraw/raw/main/aciddraw.js',
'https://raw.githubusercontent.com/CR-S01/Dungeons-Diagrams/main/dungeonsAndDesigns.js'
]

save_supported = {
'LORD':1,
'BREAKOUT':1,
'SUBLEQ':0,
'CROSSROADS':1,
'TW':0,
'MONSTERMAZE':0,
'SOKOCODE':0,
'HOOSSTARZU':0,
'HANABI':0,
'FLAPPY':0,
'EXADOOM':0,
'EMPTYSALOON':0,
'SNAKE':0,
'NETMAZE':0,
'ACIDDRAW':1,
'DDESIGNS':0,
}

friendly_names = {
'LORD':'L.O.R.D.',
'BREAKOUT':'Breakout',
'SUBLEQ':'Subleq',
'CROSSROADS':'Crossroads',
'TW':'Trade Wars 2057',
'MONSTERMAZE':'MonsterMaze',
'SOKOCODE':'SokoCode',
'HOOSSTARZU':'Hoos Starzu',
'HANABI':'Hanabi Fireworks',
'FLAPPY':'Flappy Birb',
'EXADOOM':'EXA Doom',
'EMPTYSALOON':'Empty Saloon',
'SNAKE':'Snake',
'NETMAZE':'netMAZE',
'ACIDDRAW':'ACiD Draw',
'DDESIGNS':'Dungeons&Designs'
}

def find_soko_version(door):
    expect = 'let gameVersion = \''
    f1 = door.find(expect)
    if f1 != -1:
        remains = door[f1+len(expect):-1]
        f2 = remains.find('\'')
        if f2 != -1:
            return remains[0:f2]
    return '?'

def find_exa_version(door):
    expect = 'version = \"v'
    f1 = door.find(expect)
    if f1 != -1:
        remains = door[f1+len(expect):-1]
        f2 = remains.find('\"')
        if f2 != -1:
            return remains[0:f2]
    return '?'

def find_es_version(door):
    expect1 = 'drawText(\"v'
    expect2 = ', icegoat9'
    f1 = door.find(expect1)
    f2 = door.find(expect2)
    if f1 != -1 and f2 != -1:
        return door[f1+len(expect1):f2]
    return '?'

def find_dd_version(door):
    expect = 'version = '
    f1 = door.find(expect)
    if f1 != -1:
        remains = door[f1+len(expect):-1]
        f2 = remains.find(';')
        if f2 != -1:
            return remains[0:f2]
    return '?'

def fix_hoos_getname(door):
    expect = "function getName() {\n  return 'Hoos Starzu'\n}"
    f = door.find(expect)
    if f != -1:
        top = door[0:f]
        gn = door[f:len(expect)]
        remains = door[f+len(expect):]
        gn = gn.replace('getName', 'HOOSSTARZU_getName')
        return top + gn + remains
    return None

door_divider = '\n// --------- DOOR DIVIDER --------- //\n'
protected = {}
funcs2 = []
funcs = []

def find_savedir():
    for root, dirs, files, in os.walk(dest, topdown=False):
        for name in dirs:
            s=os.path.join(root, name)
            d=s.find('\\servers')
            if d != -1: return s
    return None

def find_onedrive():
    for root, dirs, files, in os.walk(os.environ['OneDrive']+finalpath, topdown=False):
        for name in dirs:
            s=os.path.join(root, name)
            d=s.find('\\servers')
            if d != -1: return s
    return None

def mkhash(s):
    return {s: '|'+hashlib.sha1(s.encode('utf-8')).hexdigest()+'|'}

def prefix_funcs(prefix, door):
    for p in protected:
        door = door.replace(p, protected[p])
    for fun in funcs2:
        door = re.sub(r'\b'+fun, prefix+'_'+fun, door)
    for p in protected:
        door = door.replace(protected[p], p)
    return door

def chunk_report(bytes_so_far, chunk_size, total_size):
   percent = float(bytes_so_far) / total_size
   percent = round(percent*100, 2)
   sys.stdout.write("Downloaded %d of %d bytes (%0.2f%%)\r" % (bytes_so_far, total_size, percent))
   if bytes_so_far >= total_size:
      sys.stdout.write('\n')

def chunk_read(response, chunk_size=8192, report_hook=None):
   total_size = int(response.headers.get('Content-Length'))
   bytes_so_far = 0
   data = ""
   while 1:
      chunk = response.read(chunk_size)
      bytes_so_far += len(chunk)
      if not chunk:
         break
      data += chunk
      if report_hook:
         report_hook(bytes_so_far, chunk_size, total_size)
   return data

def merge_doors():
    print('Last Call BBS Classic BBS Downloader by almostsweet v'+VERSION+'\n')
    classic_bbs = ''
    bbs_data = ''
    with codecs.open('bbs.dat', 'r', encoding="utf-8") as f:
        bbs_data = f.read()
    classic_bbs += '// Classic BBS v'+VERSION+'\n'
    classic_bbs += 'let _bbs_door_methods = {\n'
    index = 0
    for key in save_supported:
        index += 1
        classic_bbs += '{0}: [{1}_onConnect, {1}_onUpdate, {1}_onInput]'.format(index,key)
        if index != len(save_supported): classic_bbs += ','
        classic_bbs += '\n'
    classic_bbs += '};\n\n'
    classic_bbs += 'let _bbs_save_supported = {\n'
    index = 0
    for key in save_supported:
        index += 1
        val = save_supported[key]
        classic_bbs += '{0}: {1}'.format(index,val)
        if index != len(save_supported): classic_bbs += ','
        classic_bbs += '\n'
    classic_bbs += '};\n\n'
    classic_bbs += 'let _bbs_door_list = ['
    index = 0
    for key in friendly_names:
        index += 1
        classic_bbs += '\'' + friendly_names[key] + '\''
        if index != len(save_supported): classic_bbs += ','
    classic_bbs += '];\n\n'
    classic_bbs += bbs_data
    door=None
    for url in door_games:
        classic_bbs += door_divider
        myresponse = urllib.request.urlopen(url)
        door = chunk_read(myresponse, report_hook=chunk_report).decode('utf-8')
        #works fine now: door = door[1:] #NUL char gets inserted for some reason, removing it
        v = door.find('@version')
        ver = '';
        if v != -1:
            ve = door.find('\n', v, v+20)
            if ve == -1: ve = v+8
            ver = door[v+9:ve]
        else:
            ver='?'
        name = os.path.basename(urlparse(url).path)
        name = os.path.splitext(name)[0].upper()
        name = name.replace('-','')
        name = name.replace('_','')
        size = len(door)
        if name == 'SOKOCODE': ver=find_soko_version(door)
        elif name == 'EXADOOM': ver=find_exa_version(door)
        elif name == 'EMPTYSALOON': ver=find_es_version(door)
        elif name == 'DABJULMAROSSNAKE': name = 'SNAKE'
        elif name == 'DUNGEONSANDDESIGNS':
            name = 'DDESIGNS'
            ver=find_dd_version(door)
        print('Door: {0:18} Version: {1:5} Size: {2}'.format(name, ver, size))
        if name == 'HOOSSTARZU':
            f = door.find('/** @type {Scene} */')
            if f != -1:
                remains = door[f:]
                remains = remains.replace('function onConnect', 'function HOOSSTARZU_onConnect')
                remains = remains.replace('function onInput', 'function HOOSSTARZU_onInput')
                remains = remains.replace('function onUpdate', 'function HOOSSTARZU_onUpdate')
                door = door[0:f] + remains
            door = fix_hoos_getname(door)
        else:
            funcs = re.findall('(?<=function )(.*?)\(', door, flags=re.MULTILINE)
            for fun in funcs:
                if ' ' not in fun and '\\' not in fun and '/' not in fun and '' != fun:
                    funcs2.append(fun)
            door = prefix_funcs(name, door)
        classic_bbs += door
    d=find_savedir()
    if d == None:
        print('\nTrying onedrive...')
        d=find_onedrive()
        if d == None:
            print('\nCould not find the save directory. Saving to current directory.')
            print('\nPlease copy the file manually to:')
            print('\n  Documents\\My Games\\Last Call BBS\\#\\servers')
            d='.'
            return
        else:
            print('Found onedrive folder...')
    print('\nSaving to: '+d+'\\classic.js')
    with codecs.open(d+'\\classic.js', 'w', encoding='utf-8') as f:
        f.writelines(classic_bbs)
    print('\nReload your NETronics and connect to the Classic BBS.')

if __name__ == '__main__':
    protected.update( mkhash('clearScreen') )
    protected.update( mkhash('JSON.stringify') )
    protected.update( mkhash('JSON.parse') )
    protected.update( mkhash('loadData') )
    protected.update( mkhash('saveData') )
    merge_doors()
